"""Basic flask app to be made kubernetes native"""
import os, sys

from flask import Flask
from flask import render_template
from flask import make_response 
from flask import session
from flask import request
from flask import redirect
from markupsafe import escape

import scryapi

app = Flask(__name__)
app.secret_key = os.urandom(16)

try:
	BUNGIEKEY = os.getenv('bungieApiKey')
except:
	raise


class HomeModel():
	states = {
		1: "guardianId",
		2: "isNotActive",
		3: "isActive",
		100: "None"
	}

	def __init__(self):
		"""Basic homepage data model"""
		self.guardianId = None
		self.state = self.states[100]
		self.currentActivity = None

	def SearchNewActivity(self):
		"""Searches new activity of player
		Sets activity information or lack there of.
		Displays activity information"""
		toon = scryapi.DestinyAPI(BUNGIEKEY)
		self.currentActivity = toon.GetCurrentUserActivities(self.guardianId)
		try:
			if self.currentActivity.currentActivityModeType is not None:
				self.state = self.states[3]
			else:
				self.state = self.states[2]
		except KeyError as e:
			e.message += "\nPotential Corrupted User Activity, missing ModeType"
			raise e


@app.route("/", methods=["GET", "POST"])
def home():	
	hm = HomeModel()
	if 'guardId' in session:
		hm.guardianId = session['guardId']
		hm.state = hm.states[1]
		print('Found it!')
	try: 
		if 'newGuardId' in session and session['newGuardId'] == 1:
			session['newGuardId'] = 0
			hm.SearchNewActivity()
	except: 
		raise

	return render_template("home.html",homemodel=hm)


@app.route("/setguardid", methods=["POST"])
def guardianid():
	"""Set cookie of guardian ID"""
	hm = HomeModel()
	if request.method == 'POST':
		try:
			guardId = request.form.get("guardianId")
			if 'guardId' in session and guardId == session['guardId']:
				pass # Skip if someone tries to add the same api call 
			elif guardId is not '':
				session['guardId'] = guardId
				session['newGuardId'] = 1
		except:
			print("Failed to set HomeModel")
			raise
	return redirect("/")

@app.route("/checkactivity", methods=["POST"])
def checkactivity():
	"""New check for activity"""
	session['newGuardId'] = 1
	return redirect("/")

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=sys.argv[1])