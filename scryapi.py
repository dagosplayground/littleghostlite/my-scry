import requests
import json
import os
 
# Not needed when using a class
 
class DestinyAPI():
 
	def __init__(self,apiKey):
		"""Andy's class to pull Destiny Information"""
		self.apiKey = {'X-API-KEY': apiKey}
 
	def GetMemberToons(self,groupID='2981133'):
		"""Function gets the members characters and playtimes.
	   groupID: Clan ID or Destiny Group ID
	   returns void"""
		classIdentifier = { 0 : 'Titan', 1 : 'Hunter', 2 : 'Warlock'}
		memberListUrl = 'https://www.bungie.net/Platform/GroupV2/' + groupID + '/Members/'
		response = requests.get(memberListUrl, headers=self.apiKey)
		json_data = resaponse.json()
		#print(json_data)
		for key in json_data['Response']['results']:
			displayName = key['destinyUserInfo']['displayName']
			personalStatsUrl = 'https://www.bungie.net/Platform/Destiny2/' + str(key['destinyUserInfo']['membershipType']) + '/Profile/' + key['destinyUserInfo']['membershipId'] + '/?components=200'
			response2 = requests.get(personalStatsUrl, headers=self.apiKey)
			json_data2 = response2.json()
			for characterData in json_data2['Response']['characters']['data'].values():
				charClass = classIdentifier[characterData['classType']]
				print('DisplayName: ' + displayName + ', Class: ' + charClass + ', TimePlayed: ' + str(characterData['minutesPlayedTotal']) + ', Light: ' + str(characterData['light']))

	def GetRawUserActivity(self, userId):
		"""Grab the activity informtion from the user."""
		# requestUrl = f'https://www.bungie.net/Platform/User/GetBungieNetUserById/{ userId }/'
		requestUrl = f'https://www.bungie.net/Platform/Destiny2/3/Profile/{ userId }/?components=100'
		response = requests.get(requestUrl, headers=self.apiKey)
		return response.json()

	def GetGeneralUserProfile(self, userId):
		"""Grab the general user profile"""
		requestUrl = f'https://www.bungie.net/Platform/Destiny2/3/Profile/{ userId }/?components=204'
		response = requests.get(requestUrl, headers=self.apiKey)
		return response.json()

	def GetUserChars(self,userId):
		"""Gets the character data of last played."""
		chars = self.GetRawUserActivity(userId)['Response']['profile']['data']['characterIds']
		return chars 

	def GetCurrentUserActivities(self,userId):
		"""Gets break out user activity
		Returns CurrentActivity Object most recent toon"""
		chars = self.GetUserChars(userId)
		rawActivities = self.GetGeneralUserProfile(userId)

		# Get list of activities
		activities = list()
		for character in chars: 
			# print(f"{character}")
			activities.append(CurrentActivity.From204(
				rawActivities["Response"]["characterActivities"]["data"][character], \
				character))
			# print(f"Char: { str(character) }\n User Data: { activities[:-1] }")

		# Compare dates of the activites 
		latest = "0000"
		latestChar = CurrentActivity() 
		for activity in activities:
			# print(f"{activity}")
			if latest < activity.dateActivityStarted:
				latest = activity.dateActivityStarted
				latestChar = activity 

		return latestChar

	def __repr__(self):
		"""Replaces the reprint string"""
		output = "APIKey: " + self.apiKey
		return output

class CurrentActivity():

	def __init__(self):
		"""Current activity Class"""
		self.characterId = str()
		self.dateActivityStarted = str()
		self.currentActivityHash = int()
		self.currentActivityModeHash = int()
		self.currentActivityModeType = int()

	@classmethod
	def From204(cls, injson,charId):
		"""Class method for creating a json object from the 204 Character Activities"""
		myclass = cls()
		try:
			myclass.characterId = charId

			myclass.dateActivityStarted = \
				str(injson["dateActivityStarted"])

			myclass.currentActivityHash = \
				int(injson["currentActivityHash"])

			myclass.currentActivityModeHash = \
				int(injson["currentActivityModeHash"])
		except:
			raise

		try: 
			myclass.currentActivityModeType = \
				int(injson["currentActivityModeType"])
		except KeyError as e:
			# **** NOTE: Key will not exist if not active
			myclass.currentActivityModeType = \
				None	
			# print(f"Activity mode type cannot be found for { charId }")
		except: 	
			raise

		return myclass

	def __repr__(self):
		"""Return variable information as dictionary"""
		retval = {
			'characterId' : self.characterId, 
			'dateActivityStarted' : self.dateActivityStarted,
			'currentActivityHash' : self.currentActivityHash, 
			'currentActivityModeHash' : self.currentActivityModeHash, 
			'currentActivityModeType' : self.currentActivityModeType
		}

		return retval
 
	def __str__(self):
		"""Returns string representation of class"""
		retval = f"'characterId' : { self.characterId}\n"
		retval += f"'dateActivityStarted' : {self.dateActivityStarted}\n"
		retval += f"'currentActivityHash' : {self.currentActivityHash}\n"
		retval += f"'currentActivityModeHash' : {self.currentActivityModeHash}\n"
		retval += f"'currentActivityModeType' : {self.currentActivityModeType}\n"

		return retval

def main():
	"""Main method allows the function to be imported elsewhere without
	calling main all the time."""

	### USER ID selection ###
	USERID = '4611686018477089571' # DagoRed
	# USERID = '4611686018475270041' # Boodah
	# USERID = '4611686018492505604' # Wallace
	# USERID = '4611686018468397005' # Jouflou


	### <\USER ID> ###

	clanToons = DestinyAPI(os.getenv('bungieApiKey'))
	# clanToons.GetMemberToons() # Default uses the value defined for groupID in the definition

	# # chardata = clanToons.GetGeneralUserProfile('4611686018477089571') # Truthless
	# chardata = clanToons.GetGeneralUserProfile(USERID)

	# with open("chardata.txt",'w') as charfile:
	# 	charfile.write(json.dumps(chardata, sort_keys=True, indent=4))

	# clanToons.GetCurrentUserActivities(USERID)
	player = clanToons.GetCurrentUserActivities(USERID)
	# print(f"Active Character:\n{ str(player) }")
	print("Player offline") if player.currentActivityModeType is None \
		else print(f"Active Character:\n{ str(player) }")

if __name__ == '__main__':
	#Only called when the script is executed from the command line directly
	main()