[[_TOC_]]

# My Scry
 
Basic 1 page webapp kubernetes app to for pulling data like Scry from Charlamegne but without 
discord. 

# Hosting

This project will build containers to be run on ARM64 variant 8 and have a build pipeline to 
publish all code from main and develop. Deployment files for kubernetes will be located in 
the k3s folder. 

